#include "fs.h"
#include <avr/eeprom.h>
#include <stdio.h>
#include <stdlib.h>

void init_eepromfs(void) {
  // Check FS
  check_fs();
  // Load blocks

}

void check_fs(void) {
  eeprom_read_block(&filetable, EEPROM, sizeof(file_table));
  if (filetable.meta.check_byte != CHECKBYTE) {
    format_fs();
  }
  if (filetable.meta.version != VERSION) {
    format_fs();
  }
}

void format_fs(void) {
  file_table f_table;
  fs_data meta = {1,85};
  f_table.meta = meta;
  eeprom_busy_wait();
  eeprom_write_block(&f_table, EEPROM, sizeof(file_table));
  filetable = f_table;
}

void open_for_write(uint8_t filename) {
  file current_file;
  if (filename > MAX_FILES || filename < 0) {
    current_file = filetable.files[filename];
  }
}
