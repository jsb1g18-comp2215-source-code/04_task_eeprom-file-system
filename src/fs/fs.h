#include <avr/io.h>

/**************************************************************************************/
/*                                  Definitions                                       */
/**************************************************************************************/


#define EEPROM 0x00
#define BLOCK_SIZE 32
#define META_SIZE 3
#define MEM_LENGTH 2048 // Give it 2KB
#define MAX_FILES 10
#define MAX_BLOCKS 8

#define VERSION 1
#define CHECKBYTE 85;

/*****************************************************************************/
/*                                  Structs                                  */
/*****************************************************************************/
typedef struct {
  uint8_t check_byte; // Used to check integrity of file system
  uint16_t blocks[MAX_BLOCKS];
  uint16_t length; // length in bytes
} file;

typedef struct {
  uint8_t version;
  uint8_t check_byte;
} fs_data;

typedef struct {
  fs_data meta;
  file files[MAX_FILES];
} file_table;

extern file_table filetable;
/*****************************************************************************/
/*                                 Functions                                 */
/*****************************************************************************/

void init_eepromfs();

void format_fs();
void check_fs();

void open_for_write(uint8_t filename);
file open_for_read(uint8_t filename);
file open_for_append(uint8_t filename);

void delete_file(uint8_t filename);

void close();
